from elasticsearch import Elasticsearch


def connect_elasticsearch():
    es = Elasticsearch([{'host': 'localhost', 'port': 9200}])
    if es.ping():
        return es
    else:
        return False


def create_document(es, index, doc_type, ID, body):
    return es.index(index=str(index), doc_type=str(doc_type), id=int(ID), body=body)


def read_document(es, index, doc_type, ID):
    return es.get(index=str(index), doc_type=str(doc_type), id=int(ID))


def update_document(es, index, doc_type, id):
    pass


def delete_document(es, index, doc_type, ID):
    return es.delete(index=str(index), doc_type=str(doc_type), id=int(ID))


def search_text(es, index, filename, text):
    return search_bool(es, index,
        {'match': {'text': text}},
        [
            {'match': {'title': filename}},
            {'match': {'_type': 'test'}}
        ],
        None,
        None
    )


def search_table(es, index, filename, text):
    return search_bool(es, index,
        {
            'match': [
                {'term': {'title': filename}},
                {'term': {'_type': 'table'}}
            ]
        },
        {'match': {'text': text}},
        {}
    )


def search_scheme(es, index, filename, text):
    return search_bool(es, index,
        {
            'match': [
                {'term': {'about': filename}},
                {'term': {'_type': 'scheme'}}
            ]
        },
        {'match': {'text': text}},
        {}
    )


def search_match(es, index, match):
    return es.search(index=str(index), body={'query': match})


def search_phrase(es, index, doc_type, phrase):
    return es.search(index=str(index),doc_type=str(doc_type),body={
        'query':{
            'match_phrase': phrase
        }
    })


def search_bool(es, index, must, filtr, should, must_not):
    return es.search(index=str(index), body={'query': {'bool': {
        'must': must,
        'filter': filtr,
        'should': should,
        'must_not': must_not
    }}})


if __name__ == '__main__':
    es = connect_elasticsearch()
    print(search_match(es, 'storage', { 'match': {'about': 'Love'}}))
    print(search_text(es, 'storage', 'Love', 'test'))
