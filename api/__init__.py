import boto3
import os
from os.path import join, dirname
from flask import Flask, flash, request, redirect, url_for, send_from_directory
from werkzeug.utils import secure_filename
from dotenv import load_dotenv
from parser import search

dotenv_path = join(dirname(__file__), '../.env')
load_dotenv(dotenv_path)

ALLOWED_EXTENSIONS = {'pdf'}
app = Flask(__name__)

UPLOAD_FOLDER = join(dirname(__file__), 'files/')

app.config['UPLOAD_FOLDER'] = join(dirname(__file__), '../files/')
app.config['MAX_CONTENT_LENGTH'] = int(os.getenv('MAX_CONTENT_LENGTH'))
app.secret_key = str(os.getenv('SECRET_KEY'))

session = boto3.session.Session()
s3 = session.client(
    service_name='s3',
    endpoint_url='https://storage.yandexcloud.net'
)
bucket = 'my-first-buck'


# Creating upload directory
def dir_check(path):
    if not os.path.exists(path):
        print('Success')
        os.makedirs(path)
        return False
    return True


# Checking for correct (allowed) file extension
def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS


# QUERING
@app.route('/query', methods=['POST'])
def process_query():
    if not request.json['component'] or not request.json['query']:
        return {'error': 'All fields must be filled in.'}
    download_object(secure_filename(request.json['component'] + '.pdf'))
    file_path = UPLOAD_FOLDER + \
        secure_filename(request.json['component'] + '.pdf')
    query = request.json['query']
    print(search(file_path, query))
    if os.path.isfile(file_path):
        # print(search(file_path, query))
        return search(file_path, query)
    return {'status': 'There is no such file.'}


# UPLOAD FILES
@app.route('/file/upload', methods=['POST'])
def upload_file():
    # check if the post request has the file part
    if 'file' not in request.files:
        return {'error': 'No file part'}
    file = request.files['file']
    if file:
        if file.filename == '':
            return {'error': 'No selected file'}
        if not allowed_file(file.filename):
            return {'error': 'Invalid extension.'}
        filename = secure_filename(file.filename)
        s3.upload_fileobj(file, bucket, filename)
        # file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
        return {'status': 'File has been saved.'}
    return {'error': 'Something went wrong.'}


# READ FILES
@app.route('/file/get/<filename>')
def uploaded_file(filename):
    if dir_check(app.config['UPLOAD_FOLDER']):
        return send_from_directory(app.config['UPLOAD_FOLDER'],
                                   filename)
    return {'status': 'There is no such file.'}


# DELETE FILES
@app.route('/file/delete', methods=['POST'])
def delete_file():
    # check if the post request has the file part
    if 'file' not in request.form:
        return {'error': 'No file part'}
        # return redirect(request.url)
    filename = request.form['file']
    if dir_check(app.config['UPLOAD_FOLDER']):
        for file in os.listdir(app.config['UPLOAD_FOLDER']):
            if file == filename + '.pdf':
                os.remove(os.path.join(app.config['UPLOAD_FOLDER'], filename))
                return {'status:': 'Removed Successfully.'}
    return {'error': 'File not found.'}


# RESPONSE HEADERS
@app.after_request
def after_request(response):
    header = response.headers
    header['Access-Control-Allow-Origin'] = 'http://localhost:3000'
    return response


def download_object(filename):
    try:
        file_path = UPLOAD_FOLDER + filename
        s3.download_file(bucket, filename, file_path)
    except Exception as err:
        print("Error: " + str(err))




if __name__ == '__main__':
    app.run(host='0.0.0.0')