import io

from pdfminer.pdfinterp import PDFResourceManager, PDFPageInterpreter
from pdfminer.pdfpage import PDFPage
from pdfminer.converter import TextConverter, PDFPageAggregator
from pdfminer.layout import LAParams, LTImage, LTFigure
from pdfminer.pdfparser import PDFParser
from pdfminer.pdfdocument import PDFDocument
import camelot

"""Returns a dictionary of table of contents of pdf document"""


def get_table_of_contents(file):
    # Open a PDF document.
    fp = open(file, 'rb')
    parser = PDFParser(fp)
    document = PDFDocument(parser)
    print(document.get_dest("Features"))
    # Get the outlines of the document.
    outlines = document.get_outlines()
    result = dict()
    for (level, title, dest, a, se) in outlines:
        info = title.split("\u2002")
        if len(info) == 2:
            result[info[0]] = info[1]

    return result


"""Converts pdf file(file) into standart output"""


def get_text(file):
    fp = open(file, 'rb')
    # Create resource manager
    rsrcmgr = PDFResourceManager()
    retstr = io.StringIO()
    codec = 'utf-8'
    laparams = LAParams()
    device = TextConverter(rsrcmgr, retstr, laparams=laparams)

    interpreter = PDFPageInterpreter(rsrcmgr, device)
    for page in PDFPage.get_pages(fp, check_extractable=True):
        interpreter.process_page(page)
    data = retstr.getvalue()
    return data


"""Extracts all tables from pdf"""


def get_tables(file):
    tables = camelot.read_pdf(file, pages="all")
    return tables


def search(file, query):
    text = get_text(file).split("\n\n")
    tables = get_tables(file)
    ret = {
        "text": [],
        "tables": []
    }

    for chunk in text:
        if query in chunk:
            ret['text'].append(chunk)

    for i in tables:
        if query in i.df.values:
            ret['tables'].append(i.df.to_json(orient='table'))

    return ret
