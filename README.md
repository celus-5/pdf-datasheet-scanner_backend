
PDF parsing engine
===
This is backend for our version of backend for PDF datasheet scanner & parser.

## How to get started
To successfully run the server, you will need to create a .env file in the project root.

```sh
MAX_CONTENT_LENGTH=<the maximum file size that you will allow users to upload to the server in bytes>
SECRET_KEY=<your secret key here>
```

И затем прописать в консоли, находясь в корне проекта:

```sh
$ pip install -r requirements.txt
$ flask run
```

Now the server is running on the 5000 port of your server.

=======
PDF Parsing engine
===
This is backend for our version of backend for PDF datasheet scanner & parser.

## How to get started

To launch this project you need .env file in the root directory of the prject with the following content:

```sh
MAX_CONTENT_LENGTH=<the maximum size of files that you want to allow users to upload>
SECRET_KEY="<secret key for your project>"
```

After that, you can install project dependencies and start it with the next commands:

```sh
pip install -r requirements.txt
flask run
```

Now the server is running on the 5000 port of your server.

## Docker

Place frontend Git repo in `frontend` directory, backend in `backend` directory.

Run on local machine
```
cd backend
docker-compose build
docker-compose pull
docker-compose up -d
```

# Cloud
To sync code to cloud run
```
rsync -e "ssh -i ~/.ssh/celus5" -avpPz ./backend ./frontend celus5@84.201.164.187:app
```

SSH to machine and go to `/home/celus5/app` directory.