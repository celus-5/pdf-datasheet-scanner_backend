import json
import os
from six.moves import urllib


class GraphQLClient:
    def __init__(self, endpoint):
        self.endpoint = endpoint
        self.token = None
        self.headername = None

    def execute(self, query, variables=None):
        return self._send(query, variables)

    def inject_token(self, token, headername='token'):
        self.token = token
        self.headername = headername

    def _send(self, query, variables):
        data = {'query': query,
                'variables': variables}
        headers = {'Accept': 'application/json',
                   'Content-Type': 'application/json'}

        if self.token is not None:
            headers[self.headername] = '{}'.format(self.token)

        req = urllib.request.Request(self.endpoint, json.dumps(data).encode('utf-8'), headers)

        try:
            response = urllib.request.urlopen(req)
            return response.read().decode('utf-8')
        except urllib.error.HTTPError as e:
            print((e.read()))
            print('')
            raise e


def get_parts(client, mpn):
    query = '''
    query get_pdf($mpn: String!) {
  		search(q:$mpn,country:"None",currency:"None", distributorapi_timeout:"3s", ){
            total
    		results{
              part{
                id
                mpn
                name
                slug
                best_datasheet{
                  page_count
                  url
                }
              } 
            }
        }
    }
    '''
    resp = client.execute(query, {'mpn': mpn})
    return json.loads(resp)


if __name__ == '__main__':
    client = GraphQLClient('https://octopart.com/apiv4/endpoint')
    client.inject_token("905bf0d2-b922-47a7-bfeb-43bde9b0f940")
    print(get_parts(client, "ST3232EBDR"))
